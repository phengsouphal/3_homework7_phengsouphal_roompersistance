package com.example.gamer.homeworkroompersistance.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;

import com.example.gamer.homeworkroompersistance.Entity.BookCategories;

@Dao
public interface CateDao {

    @Insert
    void insert(BookCategories...bookCategories);

    @Delete
    void delete(BookCategories...bookCategories);


}

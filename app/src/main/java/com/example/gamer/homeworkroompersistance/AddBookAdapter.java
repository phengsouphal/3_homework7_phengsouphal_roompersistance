package com.example.gamer.homeworkroompersistance;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.example.gamer.homeworkroompersistance.Dao.BookDataDao;
import com.example.gamer.homeworkroompersistance.Entity.BookCategories;
import com.example.gamer.homeworkroompersistance.Entity.BookData;

import static android.app.Activity.RESULT_OK;

public class AddBookAdapter extends DialogFragment implements View.OnClickListener {

    public static final int PICK_IMAGE=100;
    Uri imageUri;
   public ImageView imageView;



    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final com.example.gamer.homeworkroompersistance.Database.Database database= Room.databaseBuilder(getActivity(), com.example.gamer.homeworkroompersistance.Database.Database.class,"BookDatabase").allowMainThreadQueries().build();

        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        View view=LayoutInflater.from(getActivity()).inflate(R.layout.add_book,null);
        builder.setTitle("Donate New Book");
        builder.setView(view);
        final MyViewHolder myViewHolder=new MyViewHolder(view);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BookData bookData=new BookData(
                        myViewHolder.etTitle.getText().toString(),
                        myViewHolder.etSize.getText().toString(),
                        myViewHolder.etPrice.getText().toString(),
                        imageUri.toString(),
                        9



                );
                database.bookDataDao().insert2(bookData);
            

                Toast.makeText(getActivity(),"Save Success",Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();


    }

    @Override
    public void onClick(View v) {

    }



    class MyViewHolder{
        EditText etTitle,etSize,etPrice;
        Button btnPickImage;



        public MyViewHolder(View view){
            etTitle=view.findViewById(R.id.et_title);
            etPrice=view.findViewById(R.id.et_price);
            etSize=view.findViewById(R.id.et_size);

            imageView=view.findViewById(R.id.choose_img);
            btnPickImage=view.findViewById(R.id.btn_pick_image);

            btnPickImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openGallery();
                }
            });


        }

    }



    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK&&requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imageView.setImageURI(imageUri);

        }
    }

}

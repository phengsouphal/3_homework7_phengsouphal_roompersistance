package com.example.gamer.homeworkroompersistance.Database;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.gamer.homeworkroompersistance.Dao.BookDataDao;
import com.example.gamer.homeworkroompersistance.Dao.CateDao;
import com.example.gamer.homeworkroompersistance.Entity.BookCategories;
import com.example.gamer.homeworkroompersistance.Entity.BookData;


@android.arch.persistence.room.Database(entities = {BookData.class,BookCategories.class},version = 1)
public abstract class Database extends RoomDatabase {

    private static Database INSTANCE;
    public abstract BookDataDao bookDataDao();
    public abstract CateDao cateDao();
//    public static Database getINSTANCE(Context context)
//    {
//        if(INSTANCE==null)
//        {
//            INSTANCE=Room.databaseBuilder(context,Database.class,"BookDatabase")
//                    .allowMainThreadQueries().build();
//        }
//        return INSTANCE;
//    }


}

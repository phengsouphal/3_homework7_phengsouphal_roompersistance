package com.example.gamer.homeworkroompersistance.Entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class BookCategories {
    @PrimaryKey(autoGenerate = true)
    public int cateid;
    public String catename;

    public BookCategories(){}

    public BookCategories(String catename) {
        this.catename = catename;
    }

    public BookCategories(int cateid, String catename) {
        this.cateid = cateid;
        this.catename = catename;
    }

    public int getCateid() {
        return cateid;
    }

    public void setCateid(int cateid) {
        this.cateid = cateid;
    }

    public String getCatename() {
        return catename;
    }

    public void setCatename(String catename) {
        this.catename = catename;
    }
}

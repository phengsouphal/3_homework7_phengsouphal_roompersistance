package com.example.gamer.homeworkroompersistance.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.gamer.homeworkroompersistance.Entity.BookData;

import java.util.List;
@Dao
public interface BookDataDao {
    @Insert
    void insert(BookData bookData);

    @Insert
    void insert2(BookData ... bookDatas);

    @Update
    void update(BookData... bookData);

    @Delete
    void delete(BookData...bookData);

    @Query("SELECT * FROM bookdata")
    List<BookData> getAllBooks();

    @Query("SELECT * FROM bookdata WHERE id=:id")
    List<BookData> findBooks(int id);


}

package com.example.gamer.homeworkroompersistance;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.gamer.homeworkroompersistance.AdapterBook.AdapterBook;
import com.example.gamer.homeworkroompersistance.Dao.BookDataDao;
import com.example.gamer.homeworkroompersistance.Database.Database;
import com.example.gamer.homeworkroompersistance.Entity.BookData;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    com.example.gamer.homeworkroompersistance.Database.Database database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recycler);
        database= Room.databaseBuilder(getApplicationContext(),Database.class,"BookDatabase")
                .allowMainThreadQueries().build();

        List<BookData> bookData=database.bookDataDao().getAllBooks();

//        for (int i = 0; i <11 ; i++) {
//             bookData=new BookData("Title #"+i,"Size: "+i+"Km","Price: "+i+"00$","",i);
//            bookDataDao.insert(bookData);
//        }

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        adapter=new AdapterBook(this,  bookData);
        recyclerView.setAdapter(adapter);


        findViewById(R.id.btnDonateBook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }





    void showDialog(){
        AddBookAdapter addBookAdapter=new AddBookAdapter();
        addBookAdapter.show(getSupportFragmentManager(),"Donate New Book");
    }
}

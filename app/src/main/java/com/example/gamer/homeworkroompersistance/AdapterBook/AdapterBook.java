package com.example.gamer.homeworkroompersistance.AdapterBook;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gamer.homeworkroompersistance.Entity.BookData;
import com.example.gamer.homeworkroompersistance.R;

import java.util.List;

public class AdapterBook extends RecyclerView.Adapter<AdapterBook.MyViewHolder> {

    Context context;

    List<BookData> books;

    public AdapterBook(Context context, List<BookData> books) {
        this.context = context;
        this.books = books;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.booklists,null);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        myViewHolder.tvTitle.setText(books.get(i).getTitle());
        myViewHolder.tvSize.setText(books.get(i).getSize());
        myViewHolder.tvPrice.setText(books.get(i).getPrice());
        myViewHolder.imageView.setImageURI(Uri.parse(books.get(i).getImage()));
        myViewHolder.btnShowPop_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context,myViewHolder.btnShowPop_up);
                popupMenu.inflate(R.menu.pop_up_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit:
                                Toast.makeText(context,"Edit ...."+i,Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.remove:
                                Toast.makeText(context,"Remove ...."+i,Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.read:
                                Toast.makeText(context,"Read ...."+i,Toast.LENGTH_SHORT).show();
                                break;



                            default:
                                    break;
                        }

                        return false;


                    }
                });

                popupMenu.show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,PopupMenu.OnMenuItemClickListener {

        ImageView imageView;
        TextView tvTitle,tvSize,tvPrice;
        Button btnShowPop_up;
        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            imageView=itemView.findViewById(R.id.img);
            tvTitle=itemView.findViewById(R.id.tvtitle);
            tvSize=itemView.findViewById(R.id.tvsize);
            tvPrice=itemView.findViewById(R.id.tvprice);

            btnShowPop_up=itemView.findViewById(R.id.show_popup);





        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

                return false;

        }
    }
}

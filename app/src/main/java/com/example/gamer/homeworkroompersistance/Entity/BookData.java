package com.example.gamer.homeworkroompersistance.Entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = BookCategories.class,
                                            parentColumns = "cateid",
                                            childColumns = "id",
                                            onDelete = ForeignKey.NO_ACTION))
public class BookData {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public String title;
    public String size;
    public String price;
    public String image;
    public int cateid;

    public BookData(){}
    public BookData(String s, String toString, String string, String s1){}

    public BookData(String title, String size, String price, String image, int cateid) {
        this.title = title;
        this.size = size;
        this.price = price;
        this.image = image;
        this.cateid = cateid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCateid() {
        return cateid;
    }

    public void setCateid(int cateid) {
        this.cateid = cateid;
    }
}
